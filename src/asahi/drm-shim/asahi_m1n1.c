/*
 * Copyright (C) 2022 Alyssa Rosenzweig
 * Copyright © 2018 Broadcom
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include "drm-shim/drm_shim.h"
#include "drm-uapi/asahi_drm.h"
#include "asahi/lib/agx_pack.h"

static PyObject *p_module, *p_shim_obj, *p_ioctl, *p_bo_free;
bool drm_shim_driver_prefers_first_render_node = true;

static int
asahi_ioctl_python(int fd, unsigned long request, void *arg)
{
   PyGILState_STATE gstate = PyGILState_Ensure();

   PyObject *p_args = PyTuple_New(3);
   assert(p_args);

   PyObject *p_fd = PyLong_FromLong(fd);
   PyObject *p_request = PyLong_FromLong(request);
   PyObject *p_arg = PyLong_FromLong((intptr_t)arg);
   assert(p_fd && p_request && p_arg);

   PyTuple_SetItem(p_args, 0, p_fd);
   PyTuple_SetItem(p_args, 1, p_request);
   PyTuple_SetItem(p_args, 2, p_arg);

   PyObject *p_ret = PyObject_CallObject(p_ioctl, p_args);
   Py_DECREF(p_args);
   if (!p_ret) {
      bool do_exit = PyErr_ExceptionMatches(PyExc_KeyboardInterrupt);

      PyErr_Print();
      if (do_exit)
         exit(1);

      fprintf(stderr,"ioctl failed\n");
      PyGILState_Release(gstate);
      return -EIO;
   }

   long ret = PyLong_AsLong(p_ret);
   Py_DECREF(p_ret);

   if (PyErr_Occurred()) {
      fprintf(stderr,"ioctl returned a bad value\n");
      PyGILState_Release(gstate);
      return -EIO;
   }

   PyGILState_Release(gstate);

   if (ret < 0) {
      errno = -ret;
      return -1;
   } else {
      return ret;
   }
}

static void
asahi_bo_free(struct shim_bo *bo)
{
   PyGILState_STATE gstate = PyGILState_Ensure();

   PyObject *p_args = PyTuple_New(1);
   assert(p_args);

   PyObject *p_addr = PyLong_FromLong(bo->mem_addr);
   assert(p_addr);

   PyTuple_SetItem(p_args, 0, p_addr);

   PyObject *p_ret = PyObject_CallObject(p_bo_free, p_args);
   Py_DECREF(p_args);
   if (!p_ret) {
      bool do_exit = PyErr_ExceptionMatches(PyExc_KeyboardInterrupt);

      PyErr_Print();
      if (do_exit)
         exit(1);

      fprintf(stderr,"bo_free failed\n");
   }

   PyGILState_Release(gstate);
}


static int
asahi_ioctl_create_bo(int fd, unsigned long request, void *arg)
{
   struct shim_fd *shim_fd = drm_shim_fd_lookup(fd);
   struct drm_asahi_create_bo *create = arg;
   struct shim_bo *bo = calloc(1, sizeof(*bo));
   int ret;

   if ((ret = drm_shim_bo_init(bo, create->size)) < 0)
      return ret;

   create->offset = bo->mem_addr; /* HACK: abuse this as an input to Python */
   if ((ret = asahi_ioctl_python(fd, request, arg)) < 0) {
      drm_shim_bo_put(bo);
      return ret;
   }

   create->handle = drm_shim_bo_get_handle(shim_fd, bo);
   drm_shim_bo_put(bo);

   return ret;
}

static int
asahi_ioctl_mmap_bo(int fd, unsigned long request, void *arg)
{
   struct shim_fd *shim_fd = drm_shim_fd_lookup(fd);
   struct drm_asahi_mmap_bo *map = arg;
   struct shim_bo *bo = drm_shim_bo_lookup(shim_fd, map->handle);

   map->offset = drm_shim_bo_get_mmap_offset(shim_fd, bo);

   drm_shim_bo_put(bo);

   return 0;
}

static ioctl_fn_t driver_ioctls[] = {
   [DRM_ASAHI_SUBMIT] = asahi_ioctl_python,
   [DRM_ASAHI_WAIT] = asahi_ioctl_python,
   [DRM_ASAHI_CREATE_BO] = asahi_ioctl_create_bo,
   [DRM_ASAHI_MMAP_BO] = asahi_ioctl_mmap_bo,
   [DRM_ASAHI_GET_PARAM] = asahi_ioctl_python,
   [DRM_ASAHI_GET_BO_OFFSET] = asahi_ioctl_python,
};

void
drm_shim_driver_init(void)
{
   shim_device.bus_type = DRM_BUS_PLATFORM;
   shim_device.driver_name = "asahi";
   shim_device.driver_ioctls = driver_ioctls;
   shim_device.driver_ioctl_count = ARRAY_SIZE(driver_ioctls);
   shim_device.driver_bo_free = asahi_bo_free;

   drm_shim_override_file("DRIVER=asahi\n"
         "OF_FULLNAME=/soc/agx\n"
         "OF_COMPATIBLE_0=apple,gpu-g13g\n"
         "OF_COMPATIBLE_N=1\n",
         "/sys/dev/char/%d:%d/device/uevent", DRM_MAJOR,
         render_node_minor);

   const char *shim_module = getenv("AGX_SHIM_MODULE");

   if (!shim_module)
      shim_module = "m1n1.agx.shim";

   Py_Initialize();
   p_module = PyImport_ImportModule(shim_module);
   if (PyErr_Occurred()) PyErr_Print();
   assert(p_module && "Failed to load shim Python module");

   PyObject *p_shim = PyObject_GetAttrString(p_module, "Shim");
   if (PyErr_Occurred()) PyErr_Print();
   assert(p_shim && "Shim class not found in module");

   PyObject *p_args = PyTuple_New(1);
   assert(p_args);
   PyObject *p_fd = PyLong_FromLong(shim_device.mem_fd);
   assert(p_fd);
   PyTuple_SetItem(p_args, 0, p_fd);
   p_shim_obj = PyObject_CallObject(p_shim, p_args);
   Py_DECREF(p_shim);
   if (PyErr_Occurred()) PyErr_Print();
   assert(p_shim_obj && "Failed to instantiate Shim object");

   p_ioctl = PyObject_GetAttrString(p_shim_obj, "ioctl");
   if (PyErr_Occurred()) PyErr_Print();
   assert(p_ioctl && "ioctl method not found in shim object");
   assert(PyCallable_Check(p_ioctl) && "ioctl member not callable");

   p_bo_free = PyObject_GetAttrString(p_shim_obj, "bo_free");
   if (PyErr_Occurred()) PyErr_Print();
   assert(p_bo_free && "bo_free method not found in shim object");
   assert(PyCallable_Check(p_bo_free) && "bo_free member not callable");

   /* Release the GIL */
   PyEval_SaveThread();
}
